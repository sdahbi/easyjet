#include "../BaselineVarsllttAlg.h"
#include "../MMCDecoratorAlg.h"
#include "../MMCSelectorAlg.h"
#include "../HllttSelectorAlg.h"

using namespace HLLTT;

DECLARE_COMPONENT(BaselineVarsllttAlg)
DECLARE_COMPONENT(MMCDecoratorAlg)
DECLARE_COMPONENT(MMCSelectorAlg)
DECLARE_COMPONENT(HllttSelectorAlg)
